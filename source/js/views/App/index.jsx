import React, { Component, PropTypes } from 'react';

import Header from 'components/Header';

export default class App extends Component {
  static propTypes = {
    children: PropTypes.element.isRequired,
  }


  render() {
    const { children } = this.props;

    return (
      <div className="wrapper">
        <Header />

        <div className="Page">
          { children }
        </div>
      </div>
    );
  }
}
