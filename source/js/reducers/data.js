import { Map } from 'immutable';

import {
  REQUEST_LIST,
} from 'actions/data';

const initialState = Map({
  list: [],
  countries: [],
});

const actionsMap = {
  [REQUEST_LIST]: (state) => {
    return state.merge({
      asyncLoading: true,
      asyncError: null,
    });
  },
};

export default function reducer(state = initialState, action = {}) {
  const fn = actionsMap[action.type];
  return fn ? fn(state, action) : state;
}
