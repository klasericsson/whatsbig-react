import { combineReducers } from 'redux';
import settings from 'reducers/settings';
import data from 'reducers/data';

export default combineReducers({
  settings,
  data,
});
