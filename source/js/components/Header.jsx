import React from 'react';
import LanguageSelect from '../containers/LanguageSelect';

const Header = () => {
  return (
    <div className="site-header">
      <div className="site-header__container">
        <a className="site-header__logo" href="/">Home</a>
        <button className="site-header__menu-toggle">MENU</button>
      </div>
      <div className="site-header__language">
        <LanguageSelect />
      </div>
    </div>
  );
};

export default Header;
