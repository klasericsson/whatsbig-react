import React, { PropTypes } from 'react';

const DropDown = ({ selectedValue, items, onChange }) => {
  return (
    <div className="dropdown">
      <select value={selectedValue} onChange={onChange}>
        {
          items.map((item, i) => (
            <div key={i}>a</div>
          ))
        }
      </select>
    </div>
  );
};

DropDown.propTypes = {
  items: PropTypes.arrayOf(PropTypes.shape({
    label: PropTypes.string,
    value: PropTypes.string,
  })).isRequired,
  onChange: PropTypes.func.isRequired,
  selectedValue: PropTypes.string.isRequired,
};

DropDown.defaultProps = {
  items: [],
};

export default DropDown;
