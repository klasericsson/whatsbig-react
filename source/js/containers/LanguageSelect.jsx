import { connect } from 'react-redux';
// import { toggleTodo } from '../actions';
import DropDown from '../components/DropDown';

const mapStateToProps = (state) => {
  return {
    items: state.languages,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    onTodoClick: (id) => {
      // dispatch(toggleTodo(id));
      dispatch('xxxx');
    },
  };
};

const LanguageSelect = connect(
  mapStateToProps,
  mapDispatchToProps
)(DropDown);

export default LanguageSelect;
